from rest_framework import serializers
from backend.api import models


class Tweet(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Tweet
        fields = ('name', 'message', 'created_at', 'pk')
