from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache
from rest_framework.generics import ListCreateAPIView
from rest_framework.viewsets import ViewSet

from .models import Tweet
from backend.api import serializers

# Serve Vue Application
index_view = never_cache(TemplateView.as_view(template_name='index.html'))


class Tweets(ViewSet, ListCreateAPIView):
    """
    API endpoint that allows tweets to be viewed or created.
    """
    queryset = Tweet.objects.all().order_by('created_at', 'name')
    serializer_class = serializers.Tweet
