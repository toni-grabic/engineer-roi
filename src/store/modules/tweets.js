import tweetsService from '../../services/tweets'

const state = {
  list: []
}

const getters = {
  tweets: state => {
    return state.tweets
  }
}

const actions = {
  list ({ commit }) {
    tweetsService.list()
    .then(tweets => {
      commit('set', tweets)
    })
  },
  create({ commit }, tweet) {
    tweetsService.create(tweet)
    .then((response) => {
      commit('create', response)
    })
  }
}

const mutations = {
  set (state, tweets) {
    state.list = tweets
  },
  create(state, tweet) {
    state.list.push(tweet)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}