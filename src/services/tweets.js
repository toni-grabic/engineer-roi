import api from '@/services/api'

export default {
  list() {
    return api.get(`tweets/`)
              .then(response => response.data)
  },
  create(payload) {
    return api.post(`tweets/`, payload)
              .then(response => response.data)
  }
}